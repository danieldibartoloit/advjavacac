package tamagochi;

import java.time.Duration;
import java.time.ZonedDateTime;

public class MascotaVirtual {

    private int level;
    private boolean bored;
    private boolean hungry;
    private boolean happy;
    private ZonedDateTime boringMinutes;

    public MascotaVirtual() {
        this.level = 0;
        this.beHungry();
        this.boringMinutes = ZonedDateTime.now();
    }
    
    public void getLevel() {
        System.out.println("Mi nivel es " + this.level);
    }

    public void eat() {
        
        if (this.happy) {
            this.increaseLevel(1);
        }
        
        if (this.hungry) {
            this.beHappy();
        }

        if (this.bored && this.isMoreThan80MinutesAngry()) {
            this.beHappy();
            this.boringMinutes = ZonedDateTime.now();
        }
    }
        
    public void play() {

        if (this.happy) {
            this.increaseLevel(2);
        }

        if (this.bored) {
            this.beHappy();
        }

        if (this.hungry) {
            System.out.println("No puedo jugar tengo hambre :(");
        }
    }
    
    private boolean isMoreThan80MinutesAngry() {
        return Duration.between(this.boringMinutes, ZonedDateTime.now()).toMinutes() > 80;
    }

    private void beBored() {
        this.bored = Boolean.TRUE;
        this.happy = Boolean.FALSE;
        this.hungry = Boolean.FALSE;
    }

    private void beHappy() {
        this.happy = Boolean.TRUE;
        this.bored = Boolean.FALSE;
        this.hungry = Boolean.FALSE;
    }

    private void beHungry() {
        this.hungry = Boolean.TRUE;
        this.happy = Boolean.FALSE;
        this.bored = Boolean.FALSE;
    }
    
    private void increaseLevel(int points){
        this.level += points;
    }
}

